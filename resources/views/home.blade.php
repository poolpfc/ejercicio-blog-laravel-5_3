@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Panel de Control :D</div>
                <div class="panel-body">
                    @if(isset($edit))
                        @include('layouts.modificar')
                    @else
                        @include('layouts.formulario')
                    @endif
                    @include('layouts.tabla')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
